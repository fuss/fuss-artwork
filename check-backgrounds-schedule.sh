#!/bin/bash

echo "=== DAILY ==="
for i in fuss-artwork/wallpapers/daily/????_??_??.*; do 
	echo -n "Il file $i sarà usato "
	date -d "$(echo $i|rev|cut -d / -f 1|rev|cut -d '.' -f 1|sed -e 's/_/-/g')" "+%A %d %B %Y"
done

echo

echo "=== DAILY FOR MULTIPLE DAYS ==="
for i in fuss-artwork/wallpapers/daily/????_??_??-??.*; do
        YEAR_MONTH=$(echo $i|rev|cut -d / -f 1|rev|cut -d '.' -f 1|cut -d '_' -f 1-2|sed -e 's/_/-/g')
        DAY_MIN=$(echo $i | cut -d . -f 1 | cut -d / -f 4 | cut -d _ -f 3 | cut -d "-" -f 1)
        DAY_MAX=$(echo $i | cut -d . -f 1 | cut -d / -f 4 | cut -d _ -f 3 | cut -d "-" -f 2)
        echo -n "Il file $i sarà usato da $(date -d "$YEAR_MONTH"-"$DAY_MIN" "+%A %d %B %Y") a $(date -d "$YEAR_MONTH"-"$DAY_MAX" "+%A %d %B %Y")"
	echo
done

echo

echo "=== MONTHLY ==="
for i in fuss-artwork/wallpapers/monthly/????_??*; do 
	echo -n "Il file $i sarà usato per il mese di "
	date -d "$(echo $i|rev|cut -d / -f 1|rev|cut -d '.' -f 1|sed -e 's/_[a-z]//g'|sed -e 's/_/-/g')-01" "+%B %Y"
done

